<?php

namespace app\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\web\Response;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
//use yii\filters\auth\HttpBasicAuth;

use app\models\User;


use yii\filters\AccessControl;
 

use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;


/**
 * Default controller for the `v1` module
 */
class UserController extends ActiveController
{
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
  //      $behaviors['contentNegotiator']['formats']['text/json'] = Response::FORMAT_JSON;
        
       unset($behaviors['authenticator']);
       
            $behaviors['corsFilter'] = [
            'class' => Cors::className(),
                 'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization','X-Auth-User','X-Auth-Password'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                    
                ],
            ];
        
        $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'only' => ['index', 'update', 'update-user','set-password', 'upload',
                    'current-location', 'locations','add-pay','locations-post',
                    'geozone-in','geozone-out','logout'
                    ],
                 'except' => ['options'],   
                //'tokenParam'=>'hash',
                'authMethods' => [
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ];
 
       
         $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                    [
                        'actions' => ['index','locations'],
                        'allow' => true,
                         'verbs' => ['GET'],
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update', 'update-user','set-password', 
                                      'upload', 'current-location','add-pay',
                                      'locations-post',
                                        'geozone-in','geozone-out'
                                    ],
                        'allow' => true,
                         'verbs' => ['POST'],
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'password', 'register','logout'],
                        'allow' => true,
                         'verbs' => ['POST'],
                    ],
                    [
                        'actions' => ['delete','delete-user','test','check-phone'],
                        'allow' => true,
                //        'verbs' => ['DELETE'],
                        
                    ],
                    [
                        'actions' => ['login', 'password', 'register', 'options'],
                        'allow' => true,
                        'verbs' => ['OPTIONS'],
                        
                    ],
                 
            ],
            
        ];
         
        return $behaviors;
    }
    
    
    private $_verbs = ['POST', 'OPTIONS'];


    public function verbs() {

        $verbs = [
           'update'   => ['POST'],
           'options'   => ['options'],
         //  'delete-user'   => ['DELETE'],
        ];
        return array_merge(parent::verbs(), $verbs);
    }
    
    public $modelClass = 'app\models\User';
    
    
    
    public function actionOptions()
    {
        \Yii::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', '*');
        \Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', $this->_verbs));

    }
        
    public function Auth($username, $password){
        if(empty($username) || empty($password))
            return null;

        $user = false;
        $isPass = false;
        
        
        if($username == 'Application'){
           $user = true;
        }

        // if no record matching the requested user
        if(empty($user))
            return null;
        
        
        if ($password=='343t7NsHFFFF70iddEiq0BZ0iOU8S3xV'){
            $isPass = true;
        }

        // if password validation fails
        if(!$isPass)
            return null;

        // if user validates (both user_email, user_password are valid)
        return $user;
    }    
    
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    /**
    * @api {post} user/login  Вход в систему 
    * @apiName USER
    * @apiGroup USER
    *
    * @apiHeader {String} X-Auth-User   Application
    * @apiHeader {String} X-Auth-Password  343t7NsHFFFF70iddEiq0BZ0iOU8S3xV
    * 
    * @apiHeaderExample {json} Header-Example:
    *     {
    *       "X-Auth-User:Application",
    *       "X-Auth-Password:343t7NsHFFFF70iddEiq0BZ0iOU8S3xV",
    *       "Content-Type: application/json; charset=UTF-8"     
    *     }
    * 
    * @apiParam {String} phone Номер телефона
    * @apiParam {String} password Пароль полученный в смс
    * 
    * 
    
    */ 
     public function actionLogin(){
       $headers = \Yii::$app->request->headers;
     
       $phone = \Yii::$app->request->post('phone');
       $password = \Yii::$app->request->post('password');
       
       
        
        
        $app_username     =   $headers->get('X-Auth-User');
        $app_password     =   $headers->get('X-Auth-Password');
        
        $base = $this->Auth($app_username, $app_password);
        
        
         if ($base){
             
             
             
                    return ['status'=>200,
                            'reason'=>'Incorect password or phone',
                            'code'=>0,
                            'data'=>[
                                'token'=>0,
                            ]
                        ];
                
               
             
        } 
        
        return ['status'=>401,
                'reason'=>'Bad auth'
            ] ;
        
       
    } 
    
}


